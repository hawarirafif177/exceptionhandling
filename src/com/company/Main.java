package com.company;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Program Pembagian Bilangan");

        boolean validInput = false;
        do {
            try {
                System.out.print("Masukkan pembilang: ");
                int pembilang = scanner.nextInt();
                System.out.print("Masukkan penyebut: ");
                int penyebut = scanner.nextInt();

                int hasil = pembagian(pembilang, penyebut);
                System.out.println("Hasil pembagian: " + hasil);
                validInput = true;
            } catch (InputMismatchException e) {
                System.out.println("Input yang dimasukkan bukan bilangan bulat. Silakan coba lagi.");
                scanner.nextLine(); // Membersihkan input yang salah
            } catch (ArithmeticException e) {
                System.out.println("Terjadi kesalahan pembagian. Penyebut tidak boleh bernilai 0.");
                scanner.nextLine(); // Membersihkan input yang salah
            }
        } while (!validInput);

        scanner.close();
    }

    public static int pembagian(int pembilang, int penyebut) {
        if (penyebut == 0) {
            throw new ArithmeticException("Pembagian dengan nol tidak diperbolehkan.");
        }
        return pembilang / penyebut;
    }
}


